# PLATFORM GAME <br> [![GitLab release](https://img.shields.io/static/v1?label=Release&message=1.0.0&color=Blue)](https://gitlab.com/ArcaDan/mylibgdxproject/-/tags/1.0.0)  <img src="https://img.shields.io/badge/kotlin-%230095D5.svg?&style=for-the-badge&logo=kotlin&logoColor=white"/> <br> <img src="https://img.shields.io/badge/git%20-%23F05033.svg?&style=for-the-badge&logo=git&logoColor=white"/>

This is just one example of how it is possible to build a simple Platform Game from scratch. Only sample features are implemented in the game. In this regard it is true that the player has a sword, but for simple tests I have also implemented the shot xD. The app runs smoothly on both Desktop and Android.

Everyone can contribute to add or modify the code to maybe get a full game. So feel free to take the code, modify it at will or simply clone it to make a new one :-)

## Build With

|Name|Description|
|----|-------------------|
|Kotlin|The language used|
|libGDX|The game framework used|
|Tiled|The framework used to made maps|


# Running the game
Windows: gradlew desktop:run
Linux / macOS: ./gradlew desktop:run

## HOW TO PLAY ON COMPUTER
|COMMAND|ACTION|
|----|-------------------|
|<-|Move left|
|->|Move right|
|Z|Jump|
|X|Shoot|
|SPACE|Lock/Unlock camere on player| 
|W-A-S-D|Move camera around|  


# Just a Screenshot
[the player](https://gitlab.com/ArcaDan/mylibgdxproject/-/blob/master/android/assets/adventurer-idle-00-right.png)

[the enemy](https://gitlab.com/ArcaDan/mylibgdxproject/-/blob/master/android/assets/enemy.png)

# License
This project is licensed under the GNU General Public License v3.0 License. see the [LICENSE](https://gitlab.com/ArcaDan/mylibgdxproject/-/blob/master/LICENSE)  file for details. 